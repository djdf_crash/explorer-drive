package ua.od.djdf.exlorergoogledrive;

import android.Manifest;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.FileContent;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.ChildList;
import com.google.api.services.drive.model.ChildReference;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.ParentReference;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import ua.od.djdf.exlorergoogledrive.fragments.ContentFragment;

public class MainActivity extends AppCompatActivity
        implements EasyPermissions.PermissionCallbacks {

    public static GoogleAccountCredential mCredential;
    ProgressDialog mProgress;

    private static final int REQUEST_CODE_CAPTURE_IMAGE = 1;
    private static final int REQUEST_LOAD_IMAGE_FROM_GALLERY = 2;
    private static final int REQUEST_OPEN_FILE = 3;
    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;

    private static final int MAIN_LAYOUT = R.layout.activity_main;
    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = {DriveScopes.DRIVE_FILE};

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private FragmentManager mFragmentManager;
    private ContentFragment mContentFragment;
    private SharedPreferences mSharedPreferences;
    private Uri mUriPhotoFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProgress = new ProgressDialog(this);
        mProgress.setCancelable(false);
        mProgress.setMessage("Calling Drive API ...");


        setContentView(MAIN_LAYOUT);

        mSharedPreferences = getSharedPreferences("iD", MODE_PRIVATE);

        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mFragmentManager.beginTransaction().remove(mContentFragment).commit();
                getResultsFromApi();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        getResultsFromApi();

        mFragmentManager = getSupportFragmentManager();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                break;
            case R.id.camera:
                Intent intentImageCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageFileName = "JPEG_" + timeStamp + "_";
                java.io.File storageDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES);
                java.io.File imageFile = null;
                try {
                    imageFile = java.io.File.createTempFile(
                            imageFileName,  /* prefix */
                            ".jpg",         /* suffix */
                            storageDir      /* directory */
                    );
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mUriPhotoFile = Uri.fromFile(imageFile);

                if (imageFile != null) {
                    intentImageCapture.putExtra(MediaStore.EXTRA_OUTPUT, mUriPhotoFile);
                    startActivityForResult(intentImageCapture, REQUEST_CODE_CAPTURE_IMAGE);
                }
                break;
            case R.id.openGallery:
                Intent i = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, REQUEST_LOAD_IMAGE_FROM_GALLERY);

                break;
            case R.id.openFile:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("file/*");
                startActivityForResult(intent, REQUEST_OPEN_FILE);

                break;
        }
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("file_uri", mUriPhotoFile);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mUriPhotoFile = savedInstanceState.getParcelable("file_uri");
        }
    }

    private void getResultsFromApi() {
        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else if (!isDeviceOnline()) {
            Toast.makeText(this, "No network connection available.", Toast.LENGTH_LONG).show();
        } else {
            new MakeRequestTask(mCredential).execute();
        }
    }


    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount() {
        if (EasyPermissions.hasPermissions(
                this, Manifest.permission.GET_ACCOUNTS)) {
            String accountName = getPreferences(Context.MODE_PRIVATE)
                    .getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null) {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromApi();
            } else {
                // Start a dialog from which the user can choose an account
                startActivityForResult(
                        mCredential.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER);
            }
        } else {
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    Toast.makeText(this,
                            "This app requires Google Play Services. Please install " +
                                    "Google Play Services on your device and relaunch this app.", Toast.LENGTH_LONG).show();
                } else {
                    getResultsFromApi();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        getResultsFromApi();
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    getResultsFromApi();
                }
                break;
            case REQUEST_CODE_CAPTURE_IMAGE:
                if (resultCode == RESULT_OK) {
                    if (mUriPhotoFile != null) {
                        String picturePath = mUriPhotoFile.getPath();
                        String titleFile = picturePath.split(java.io.File.separator)[picturePath.split(java.io.File.separator).length - 1];
                        String desc = "";
                        String rootId = mSharedPreferences.getString(mCredential.getSelectedAccountName(), "");
                        String mimeType = this.getContentResolver().getType(mUriPhotoFile);
                        insertFile(titleFile, desc, rootId, mimeType, picturePath);
                    }

                }
                break;
            case REQUEST_LOAD_IMAGE_FROM_GALLERY:
                if (resultCode == RESULT_OK && data != null) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);

                    if (cursor == null || cursor.getCount() < 1) {
                        return;
                    }

                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

                    if (columnIndex < 0)
                        return;

                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    String titleFile = picturePath.split(java.io.File.separator)[picturePath.split(java.io.File.separator).length - 1];
                    String desc = "";
                    String rootId = mSharedPreferences.getString(mCredential.getSelectedAccountName(), "");
                    String mimeType = this.getContentResolver().getType(data.getData());
                    insertFile(titleFile, desc, rootId, mimeType, picturePath);
                }

                break;
            case REQUEST_OPEN_FILE:
                if (resultCode == RESULT_OK && data != null) {
                    Uri selectedFile = data.getData();
                    String titleFile = data.getData().getPath().split(java.io.File.separator)[data.getData().getPath().split(java.io.File.separator).length - 1];
                    String desc = "";
                    String rootId = mSharedPreferences.getString(mCredential.getSelectedAccountName(), "");
                    String mimeType = this.getContentResolver().getType(data.getData());
                    String fileName = selectedFile.getPath();
                    insertFile(titleFile, desc, rootId, mimeType, fileName);
                }

                break;
        }
    }


    private void insertFile(final String title, final String description, final String parentId, final String mimeType, final String filename) {

        if (isDeviceOnline()) {
            new AsyncTask<Void, File, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    com.google.api.services.drive.Drive mService = ServiceAPI.getServiceAPI(mCredential);

                    // File's metadata.
                    File body = new File();
                    body.setTitle(title);
                    body.setDescription(description);
                    body.setMimeType(mimeType);

                    // Set the parent folder.
                    if (parentId != null && parentId.length() > 0) {
                        body.setParents(Arrays.asList(new ParentReference().setId(parentId)));
                    }

                    java.io.File fileContent = new java.io.File(filename);
                    FileContent mediaContent = new FileContent(mimeType, fileContent);
                    try {
                        File file = mService.files().insert(body, mediaContent).execute();
                        publishProgress(file);
                    } catch (IOException e) {
                        Toast.makeText(getApplicationContext(), "An error occured: " + e, Toast.LENGTH_LONG).show();
                    }
                    return null;
                }

                @Override
                protected void onProgressUpdate(File... values) {
                    super.onProgressUpdate(values);
                    mContentFragment.getFileList().add(0, values[0]);
                    mContentFragment.getmResultsAdapter().notifyItemChanged(0);
                }
            }.execute();
        }else {
            Toast.makeText(this,"No network connection available.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Do nothing.
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Do nothing.
    }

    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }


    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }


    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }


    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                MainActivity.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }


    private class MakeRequestTask extends AsyncTask<Void, Void, List<File>> {
        private com.google.api.services.drive.Drive mService = null;
        private Exception mLastError = null;
        private GoogleAccountCredential credential;
        private String rootFolderId;

        public MakeRequestTask(GoogleAccountCredential credential) {
            this.credential = credential;
            mService = ServiceAPI.getServiceAPI(credential);
        }


        @Override
        protected List<File> doInBackground(Void... params) {
            try {
                return getDataFromApi();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }


        private List getDataFromApi() throws IOException {
            if (TextUtils.equals(mSharedPreferences.getString(credential.getSelectedAccountName(), ""), "")) {
                About about = mService.about().get().execute();
                rootFolderId = about.getRootFolderId();
                mSharedPreferences.edit().putString(credential.getSelectedAccountName(), rootFolderId).apply();
            } else {
                rootFolderId = mSharedPreferences.getString(credential.getSelectedAccountName(), "");
            }


            com.google.api.services.drive.Drive.Children.List request = mService
                    .children()
                    .list(rootFolderId)
                    .setQ("trashed=false");
            List<File> files = new ArrayList<>();
            do {
                try {
                    ChildList children = request.execute();
                    for (ChildReference child : children.getItems()) {
                        File file = mService.files().get(child.getId()).execute();
                        files.add(file);
                    }
                    request.setPageToken(children.getNextPageToken());
                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(), "An error occurred: " + e, Toast.LENGTH_LONG).show();
                    request.setPageToken(null);
                }
            } while (request.getPageToken() != null &&
                    request.getPageToken().length() > 0);

            return files;
        }


        @Override
        protected void onPreExecute() {
            mProgress.show();
        }

        @Override
        protected void onPostExecute(List<File> output) {
            mProgress.hide();
            if (output == null || output.size() == 0) {
                Toast.makeText(getApplicationContext(), "No results returned.", Toast.LENGTH_LONG).show();
            } else {
                mContentFragment = new ContentFragment();
                mContentFragment.setTAG(rootFolderId);
                mFragmentManager.beginTransaction()
                        .add(R.id.container, mContentFragment)
                        .commit();
                mContentFragment.setFileList(output);
            }
        }

        @Override
        protected void onCancelled() {
            mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            MainActivity.REQUEST_AUTHORIZATION);
                } else {
                    Toast.makeText(getApplicationContext(), "The following error occurred:\n"
                            + mLastError.getMessage(), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Request cancelled.", Toast.LENGTH_LONG).show();
            }
        }
    }
}