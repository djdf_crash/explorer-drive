package ua.od.djdf.exlorergoogledrive.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.api.services.drive.model.File;

import java.io.Serializable;
import java.util.List;

import ua.od.djdf.exlorergoogledrive.R;
import ua.od.djdf.exlorergoogledrive.ResultsAdapter;


public class ContentFragment extends Fragment {

    private ResultsAdapter mResultsAdapter;

    private List<File> fileList;
    private String TAG;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_content, container, false);
        if (savedInstanceState != null){
            setFileList((List<File>) savedInstanceState.getBundle("listFiles").getSerializable("listFiles"));
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(2, 1);
        RecyclerView mRecycleView = (RecyclerView) getActivity().findViewById(R.id.files_recycler_view);
        mResultsAdapter = new ResultsAdapter(getActivity());
        mResultsAdapter.setmFragmentManager(getFragmentManager());
        mRecycleView.setLayoutManager(mLayoutManager);
        mResultsAdapter.setListFiles(getFileList());
        mRecycleView.setAdapter(mResultsAdapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle stateList = new Bundle();
        stateList.putSerializable("listFiles", (Serializable) getFileList());
        outState.putBundle("listFiles", stateList);
    }

    public void setFileList (List<File> output){
        this.fileList = output;
    }

    public List<File> getFileList (){
        return this.fileList;
    }

    public ResultsAdapter getmResultsAdapter() {
        return mResultsAdapter;
    }

    public String getTAG() {
        return TAG;
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }
}
