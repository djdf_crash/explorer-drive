package ua.od.djdf.exlorergoogledrive;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class ServiceAPI {
    public static com.google.api.services.drive.Drive getServiceAPI(GoogleAccountCredential credential) {
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        return new com.google.api.services.drive.Drive.Builder(
                transport, jsonFactory, credential)
                .setApplicationName("Explorer Google Drive")
                .build();
    }

    public static GoogleAccountCredential getCredential() {
        return MainActivity.mCredential;
    }

    public static class DownloadFile extends AsyncTask<Void, Void, File> {

        private com.google.api.services.drive.model.File mFile;
        private com.google.api.services.drive.Drive service;
        private Context context;
        private Exception mLastError;
        private boolean send;

        public DownloadFile(Context context, com.google.api.services.drive.model.File file, boolean send) {
            this.context = context;
            this.mFile = file;
            this.send = send;
        }

        @Override
        protected File doInBackground(Void... params) {
            try {
                return getOutputStream();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        public File getOutputStream() throws Exception {
            service = getServiceAPI(getCredential());
            File path = new File(Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DOWNLOADS);
            File tmpFile = File.createTempFile(
                    mFile.getTitle().substring(0, mFile.getTitle().length() - 4),
                    "." + mFile.getFileExtension(),
                    path);
            FileOutputStream outputStream = new FileOutputStream(tmpFile);

            if (mFile.getDownloadUrl() != null && mFile.getDownloadUrl().length() > 0) {
                try {
                    HttpResponse resp = service.getRequestFactory()
                            .buildGetRequest(new GenericUrl(String.valueOf(Uri.parse(mFile.getDownloadUrl()))))
                            .execute();
                    resp.download(outputStream);
                } catch (IOException e) {
                    // An error occurred.
                    e.printStackTrace();
                    return null;
                }
            }

            outputStream.flush();
            outputStream.close();

            return tmpFile;
        }

        @Override
        protected void onPostExecute(File outputFile) {
            super.onPostExecute(outputFile);
            Intent fileIntent = new Intent();
            fileIntent.setAction(send ? Intent.ACTION_SEND : Intent.ACTION_VIEW);
            if (!send) {
                fileIntent.setDataAndType(Uri.fromFile(outputFile), mFile.getMimeType());
                fileIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            } else {
                fileIntent.setType(mFile.getMimeType());
                fileIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(outputFile));
            }
            context.startActivity(Intent.createChooser(fileIntent, send ? "Send file to:" : "Open file in:"));
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (mLastError != null) {
                Toast.makeText(context, "The following error occurred:\n"
                        + mLastError.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
