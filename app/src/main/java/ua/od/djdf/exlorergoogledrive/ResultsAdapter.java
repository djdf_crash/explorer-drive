package ua.od.djdf.exlorergoogledrive;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.services.drive.model.File;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ResultsAdapter extends RecyclerView.Adapter<ResultsAdapter.CustomViewHolder> {

    private Context mContext;
    private List<File> mListFiles;
    private static final String MIME_FOLDER = "application/vnd.google-apps.folder";
    private FragmentManager mFragmentManager;

    public ResultsAdapter(Context context) {
        this.mContext = context;
    }

    public void setListFiles(List<File> mListFiles) {
        this.mListFiles = mListFiles;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_file, parent, false);
            return new FileViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_folder, parent, false);
            return new FolderViewHolder(v);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (TextUtils.equals(mListFiles.get(position).getMimeType(), MIME_FOLDER)) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        File file = mListFiles.get(position);
        if (holder.getItemViewType() == 1) {
            ((FolderViewHolder) holder).nameFolder.setText(file.getTitle());
            //Picasso.with(mContext).load(file.getIconLink()).resize(35, 35).into(holder.filePhoto);
            ((FolderViewHolder) holder).folderPhoto.setImageResource(R.drawable.ic_folder_open_black_24dp);
        } else {
            //holder.filePhoto.setImageResource(R.drawable.ic_file_black_24dp);
            ((FileViewHolder) holder).nameFile.setText(file.getTitle());
            if (file.getThumbnailLink() != null) {
                Picasso.with(mContext).load(file.getThumbnailLink()).into(((FileViewHolder) holder).filePhoto);
            } else {
                Picasso.with(mContext).load(file.getIconLink()).resize(60, 60).into(((FileViewHolder) holder).filePhoto);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mListFiles.size();
    }

    public FragmentManager getmFragmentManager() {
        return mFragmentManager;
    }

    public void setmFragmentManager(FragmentManager mFragmentManager) {
        this.mFragmentManager = mFragmentManager;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        public CustomViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class FileViewHolder extends CustomViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView nameFile;
        private ImageView filePhoto;

        public FileViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            nameFile = (TextView) itemView.findViewById(R.id.nameFile);
            filePhoto = (ImageView) itemView.findViewById(R.id.file_photo);
        }


        @Override
        public void onClick(View v) {
            //Download in cash and open
            new ServiceAPI.DownloadFile(mContext, mListFiles.get(getAdapterPosition()), false).execute();

            Toast.makeText(mContext, "Is not folder", Toast.LENGTH_SHORT).show();
        }

        @Override
        public boolean onLongClick(View v) {

            new ServiceAPI.DownloadFile(mContext, mListFiles.get(getAdapterPosition()), true).execute();

            return true;
        }
    }

    public class FolderViewHolder extends CustomViewHolder implements View.OnClickListener {
        private TextView nameFolder;
        private ImageView folderPhoto;

        public FolderViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            nameFolder = (TextView) itemView.findViewById(R.id.nameFolder);
            folderPhoto = (ImageView) itemView.findViewById(R.id.folder_photo);
        }

        @Override
        public void onClick(View v) {

            new LoaderFiles(mContext, mListFiles.get(getAdapterPosition()).getId(), getmFragmentManager()).execute();
        }

    }
}
