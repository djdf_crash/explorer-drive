package ua.od.djdf.exlorergoogledrive;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.google.api.services.drive.model.ChildList;
import com.google.api.services.drive.model.ChildReference;
import com.google.api.services.drive.model.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ua.od.djdf.exlorergoogledrive.fragments.ContentFragment;


public class LoaderFiles extends AsyncTask<Void, Void, List<File>> {

    private com.google.api.services.drive.Drive mService = null;
    private String folderId;
    private Context context;
    private ProgressDialog progressDialog;
    private FragmentManager fragmentManager;

    public LoaderFiles(Context context, String folderId, FragmentManager fragmentManager) {
        this.folderId = folderId;
        this.context = context;
        this.fragmentManager = fragmentManager;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Calling Drive API ...");
        progressDialog.setCancelable(false);
        mService = ServiceAPI.getServiceAPI(MainActivity.mCredential);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.show();
    }

    @Override
    protected List<File> doInBackground(Void... params) {
        try {
            return getData();
        } catch (IOException e) {
            cancel(true);
            return null;
        }
    }

    public List<File> getData() throws IOException {
        com.google.api.services.drive.Drive.Children.List request = mService
                .children()
                .list(folderId)
                .setQ("trashed=false");
        List<File> files = new ArrayList<>();
        do {
            try {
                ChildList children = request.execute();
                for (ChildReference child : children.getItems()) {
                    File file = mService.files().get(child.getId()).execute();
                    files.add(file);
                }
                request.setPageToken(children.getNextPageToken());
            } catch (IOException e) {
                Toast.makeText(context, "An error occurred: " + e, Toast.LENGTH_LONG).show();
                request.setPageToken(null);
            }
        } while (request.getPageToken() != null &&
                request.getPageToken().length() > 0);

        return files;
    }

    @Override
    protected void onPostExecute(List<File> files) {
        super.onPostExecute(files);
        progressDialog.hide();
        ContentFragment contentFragment = new ContentFragment();
        contentFragment.setTAG(folderId);

        fragmentManager.beginTransaction()
                //.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                .replace(R.id.container, contentFragment)
                .addToBackStack(null)
                .commit();

        contentFragment.setFileList(files);
    }
}
